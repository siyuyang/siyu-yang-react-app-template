const path = require('path');

module.exports = {
  entry: path.resolve(__dirname, './src/index.js'),
  module: {
    rules: [
      {
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        options: {
          presets: [
            '@babel/preset-env',
            '@babel/preset-react',
            '@babel/preset-flow',
          ],
          // plugins: ['@babel/plugin-proposal-object-rest-spread']
        },
        test: /\.js$/,
      },
      {
        loader: 'css-loader',
        test: /\.css$/,
      },
    ],
  },
  output: {
    filename: '[name].min.js',
    path: path.resolve(__dirname, './dist'),
  },
};
